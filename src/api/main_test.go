package main

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"
)

func TestHandleWeather(t *testing.T) {
	saveIpAddr = func(db *sql.DB, ipAddr string, timestamp time.Time) error {
		return nil
	}

	type test struct {
		url  string
		want int
	}

	tests := []test{
		{"/api/v1/weather?lat=55.5600&lon=13.0247", http.StatusOK},
		{"/api/v1/weather?zip=94040", http.StatusOK},
		{"/api/v1/weather?invalid=something", http.StatusBadRequest},
	}

	for _, tc := range tests {
		req, err := http.NewRequest("GET", tc.url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(HandleWeather)
		handler.ServeHTTP(rr, req)

		got := rr.Code
		if !reflect.DeepEqual(tc.want, got) {
			t.Fatalf("expected: %v, got: %v", tc.want, got)
		}
	}

}
