module api

go 1.17

require (
	db v1.0.0
	models v1.0.0
)

replace (
	db v1.0.0 => ../db
	models v1.0.0 => ../models
)
