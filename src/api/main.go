package main

import (
	"database/sql"
	"db"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"models"
	"net/http"
	"net/url"
	"time"
)

var dbInstance *sql.DB
var saveIpAddr = db.SaveIpAddr

func makeWeatherRequest(params map[string]string) []byte {
	var requestUrl string
	if zip, ok := params["zip"]; ok {
		requestUrl = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?zip=%v&appid=5d1bd702e046563af6b48bc76acdfcbb", zip)
	} else {
		requestUrl = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%v&lon=%v&appid=5d1bd702e046563af6b48bc76acdfcbb", params["lat"], params["lon"])
	}
	resp, err := http.Get(requestUrl)
	if err != nil {
		log.Fatalln(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return body
}

func validateParams(params url.Values) (map[string]string, bool) {
	validParams := make(map[string]string)
	if len(params["lat"]) == 1 && len(params["lon"]) == 1 {
		validParams["lat"], validParams["lon"] = params["lat"][0], params["lon"][0]
		return validParams, true
	}
	if len(params["zip"]) == 1 {
		validParams["zip"] = params["zip"][0]
		return validParams, true
	}
	return validParams, false
}

func populateWeatherResponse(responseMap models.OwmResponse, wr *models.WeatherResponse) {
	wr.Location = responseMap.Name
	wr.Weather = responseMap.Weather[0].Description
	wr.Temperature = math.Round((responseMap.Main.Temp-273.15)*10) / 10
	wr.QueriedAt = time.Now().Local()
}

func HandleWeather(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: weather")

	if r.Method != "GET" {
		_, err := fmt.Fprintf(w, "Sorry, only GET method is supported for weather endpoint")
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	params := r.URL.Query()
	validParams, areValid := validateParams(params)
	if !areValid {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid query parameters."))
		if err != nil {
			log.Fatalln(err)
		}
		return
	}
	resp := makeWeatherRequest(validParams)

	err := saveIpAddr(dbInstance, r.RemoteAddr, time.Now().Local())
	if err != nil {
		log.Fatalln(err)
	}

	var responseMap models.OwmResponse
	err = json.Unmarshal(resp, &responseMap)
	if err != nil {
		log.Fatalln(err)
	}

	wr := models.WeatherResponse{}
	populateWeatherResponse(responseMap, &wr)

	data, _ := json.Marshal(wr)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(data)
	if err != nil {
		log.Fatalln(err)
	}
}

func handleRequests() {
	http.HandleFunc("/api/v1/weather", HandleWeather)
	log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
	dbInstance = db.Connect()
	handleRequests()
	defer db.Disconnect(dbInstance)
}
