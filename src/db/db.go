package db

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

const (
	DSN = "postgres://postgres:postgres@localhost:5432/weather_api?sslmode=disable"
)


func Connect() *sql.DB {
	db, err := sql.Open("postgres", DSN)
	if err != nil {
		log.Fatalln(err)
	}
	return db
}

func Disconnect(db *sql.DB) {
	err := db.Close()
	if err != nil { log.Fatalln(err) }
}

func SaveIpAddr(db *sql.DB, ipAddr string, timestamp time.Time) error {
	sqlStatement := `
			-- noinspection SqlResolve
			INSERT INTO api_calls (ip_addr, timestamp)
			VALUES ($1, $2)
			RETURNING id`
	var id int
	err := db.QueryRow(sqlStatement, ipAddr, timestamp).Scan(&id)
	if err != nil {
		log.Fatalln(err)
		return err
	}
	fmt.Println("Added new API call, ID is:", id)
	return nil
}