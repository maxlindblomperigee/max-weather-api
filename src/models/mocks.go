package models

import "database/sql"

type MockDBRepo struct {}

func (mdb *MockDBRepo) Connect() *sql.DB {
	return nil
}

func (mdb *MockDBRepo) Disconnect() {
	return
}

func (mdb *MockDBRepo) SaveIpAddr() error {
	return nil
}