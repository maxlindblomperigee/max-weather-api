## Known issues

- Tests are incomplete. All existing tests are valid but does not cover enough. For example does not test:
  - Database (at all)
  - What happens when request to openweather fails?
  - Invalid values for query params
- App will panic if openweather api can't find the city (inputting an incorrect zip for instance), because the response will look different
- Solution not hosted, only local
- General error handling - what to do if something fails?